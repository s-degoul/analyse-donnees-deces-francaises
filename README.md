
Import des données administratives de décès en France.

# Source

INSERM

[Fichiers des personnes décédées](https://www.data.gouv.fr/fr/datasets/fichier-des-personnes-decedees/)

# Utilisation, pour obtenir des données *data managées*

1) Créer deux répertoires :

- `donnees`
- `cache`

2) Télécharger manuellement les fichiers dans le répertoire `donnees`.

3) Fichier `request.R` :

- adapter les paramètres de configuration en début de fichier
- utiliser la fonction `import.data()`


Depuis un script quelconque :

``` R
source("chemin_vers_repertoire_deces/request.R")
deces <- import.data() # on peut préciser les paramètres start.date et end.date
# Faire quelque chose avec ses données contenues dans le dataframe "deces"...
```

# Un exemple : évolution des décès par tranche d'âge

"Knitter" le fichier `death_age.Rmd` avec *rmarkdown* (logiciel *RStudio* par exemple)

